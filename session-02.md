# Plan

# Petit détour dans les entrailles de git

* sha1sum LISEZ_MOI.txt
* rajouter point d'exclamantion
* sha1sum LISEZ_MOI

* créer script.py
* sha1sum script.py

* sha1sum LISEZ_MOI script.py
* créer un inventaire
* sha1sum inventaire

* changer script.py
* re-génére inventaire
* sha1sum inventaire: différent

* créer commit avec nom, date et sha1sum de l'inventaire
Premièrement, vous *devez* configurer git avec votre
nom et email:

```
$ git config --global user.name "votre nom"
$ git config --global user.email "votre email"
```

# Démo1

Très similaire à la démo de y a 15 jours mais en plus réaliste

* Ajout ssh
* Clonez le projet pense-bete

```
$ git clone https://git.dmerej.info/e2l/pense-bete.git
```

* git log, git log -p

* Constatez l'erreur dans les fichier LISEZ_MOI
* Création d'une branche `git checkout -b correction-lisez-moi`
* Git add
* git commit
* Monter gitk
* git push origin correction-lisez-moi


# Démo2: coder et faire des commits en même temps

* Faire deux trois modifs
* git gui to show staged/index
* git push
* git reset --hard pour undo
* git cherry-pick
* git push -f
* or
* git rebate -i for super-powers

# Démo3: merge et conflits

* Projet from scratch
* Ajout dmerej en collaborateur
* Merge request from dmerej sans conflit
* git fetch
* git merge
* (notez -> pull c'est fetch+merge dans les cas simples)

* Création d'un conflit
* Résolution

# Fin

Pour aller plus loin
* autres usages de checkout
* filter-branch
* bissect
* etc ...
* à vous de regarder
* alias
* etc ...


