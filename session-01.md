# Plan

* un peu de bla bla
* pendant ce temps:
  * installez git et crééz un compte sur GitLab.com si ce n'est pas déjà fait
  * laissez moi vous ajouter au groupe e2l si ce n'est pas déjà fait

# Intro

partager du code c'est compliqué - je vous vois
échanger les codes par e-mail c'est pas pratique

ensuite je réponds à vos emails en mettant des commentaires
à côté du code.

enfin, dans un autre contexte vous avez peut-être déjà
essayé de produire un document à plusieurs, et vous
vous êtes retrouvés avec plusieurs versions
 document1, document2, document-final, document-vraiment-final-5

gestion de versions:

* enregistrer les changements d'un fichier or d'un ensemble de fichier au cours du temps
* voir qui a changé quoi et quand
* revenir à une version antérieure (un peu comme les sauvegardes d'un jeu
  vidéo) - ou comme si on avait un 'ctrl-z'  infini
* .. etc


# Petite histoire des logiciels de gestion de versions

*local*
*central*
*distributed*

# Git

* Succession d'instantanés appelés "commits" *commits*
* Tout se passe *en local* -> par example, pas besoin du
  serveur pour consulter l'historique *en entier*, ou
  chercher dedans.
* Tout ce qui est stocké est *hashé*
* Git ne fait j'amais *qu'ajouter* de la donnée, il
  en supprime vraiment très rarement -> c'est très
  dur de perdre de la donnée avec Git (et tant mieux)
* Les fichiers on un cycle de vie en 3 états *états*

# Début atelier

rappels

* `cd` - avec et sans arguments
* `ls`
* `mkdir`

Premièrement, vous *devez* configurer git avec votre
nom et email:

```
$ git config --global user.name "votre nom"
$ git config --global user.email "votre email"
```

* Créez un  compte sur GitLab
* Ajout clé ssh

```
$ git init
$ Ajouters un fichier quelconque
$ git add .
$ git commit -m "mon premier commit"
$ git remote add ....
$ git push
```

* Laissez-moi changer des trucs

```
$ git pull
```

* Créez un tag

```
$ git tag
```

# Historique

*historique*

`gitk --all`

# Branche

* Création avec `git checkout -b mabranche`
* Modifiez le fichier que je viens d'ajouter et poussez
  la branche

* Créez une pull request
